import { Meteor } from 'meteor/meteor';
import React from 'react';
import ViewModel from 'viewmodel-react';
import { createContainer } from 'meteor/react-meteor-data';

import { Games } from '/imports/collections/games.js';
import { Content } from '../components/Content.js';

const Container = createContainer(() => {
  const dataHandle = Meteor.subscribe('gamesForPortal');
  const games = Games.find().fetch();
  const dataExists = !!games;
  
  return {
    games: dataExists? games : [],
    dataExists
  };
}, Content);

export { Container };