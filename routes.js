import { FlowRouter } from 'meteor/kadira:flow-router';
import React from 'react';
import { mount } from 'react-mounter';

import { PageLayout } from '/imports/ui/PageLayout.js';

import { Container } from '/imports/ui/containers/Container.js';


FlowRouter.route('/', {
  name: 'tGames',
  action() {
    mount(
      PageLayout, { content: <Container /> }
    );
  }
});

