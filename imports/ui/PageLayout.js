import React from 'react';
import ViewModel from 'viewmodel-react';


const PageLayout = ({ content }) => (
  <div className = "page_wrap">
    
    {content}
    
  </div>
);


export { PageLayout };