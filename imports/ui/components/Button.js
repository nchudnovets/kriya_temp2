import { FlowRouter } from 'meteor/kadira:flow-router';
import React from 'react';
import { Component } from 'react';
import ViewModel from 'viewmodel-react';

Button({
  
  render(){
    return(
      <button 
        class={this.parent().isClicked()? 'clicked': ''}
        b="toggle: parent.isClicked"
      >
        Click Me
      </button>
    );
  }
});

export { Button };
