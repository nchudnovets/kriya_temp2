import { Mongo } from 'meteor/mongo';

const Games = new Mongo.Collection('games');


Meteor.startup(()=>{
  if(Meteor.isServer){ 
    if(Games.find().count() === 0){
      _(2).times((i)=>{
        console.log('game adding!!!');
        let name = `Game ${i+1}`;
        let data = {
          title: name,
          goal: `${i+1} - Lorem ipsum dolor sit amet, consectetur adipiscing elit.`
        };
    
        Games.insert(data, (err, id)=>{
          if(err){
            console.log(err);
          }
          else{
            console.log('game added');
          }
        });
    });
      console.log("fake games data added!");
    }
  }
});

export { Games };

